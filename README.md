# Ta'anga Ñe'ẽ
Proyecto desarrollado para el concurso de aplicaciones móviles de la ETYC 2019
## About
Este proyecto consiste de un traductor de palabras 
De guaraní a castellano y de castellano a guaraní, que muestra tanto el significado de la 
palabra como una imagen descriptiva del concepto.

## Link de la apk
https://drive.google.com/file/d/1bgTFCxbt02iO86eT7HRwK6fct3rEufLp/view?usp=sharing

## Integrantes

-Cristhian David González Prieto
-Luis Alejandro Alvarez Cristaldo