import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text("Ta'anga Ñe'e"),
          ),
          body: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: const EdgeInsets.fromLTRB(40.0, 40.0, 40.0, 12.0),
                  decoration: const BoxDecoration(
                    border: Border(
                      top: BorderSide(
                          width: 1.0, color: Color.fromRGBO(50, 205, 50, 5)),
                      left: BorderSide(
                          width: 1.0, color: Color.fromRGBO(50, 205, 50, 5)),
                      right: BorderSide(
                          width: 1.0, color: Color.fromRGBO(50, 205, 50, 5)),
                      bottom: BorderSide(
                          width: 1.0, color: Color.fromRGBO(50, 205, 50, 5)),
                    ),
                    color: Color.fromRGBO(220, 220, 220, 5),
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  child: Text(
                    "Traducir castellano al guarani",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 32,
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
