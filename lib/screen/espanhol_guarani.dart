import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:taanga_nhee/searchForm.dart';

int _selectedIndex = 0;
final db = Firestore.instance;
String link_imagen = '';
String _traduccion_guarani = '';
String link_imagen_no_disponible =
    'https://www.hongshen.cl/wp-content/uploads/2016/07/no-disponible.png';
SearchForm buscador = SearchForm();

class Espanhol_guarani extends StatelessWidget {
  //const Espanhol_guarani({Key key}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.lightGreen,
      ),
      home: MyHomePage(title: "Ta'anga Ñe'ẽ"),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  final textController = new TextEditingController();
  final formKey = GlobalKey<FormState>();
  String _palabraBuscada;

  void _changeCounter(int index) {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      //print(index);
      if (index == 0) {
        _counter++;
      } else {
        _counter--;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return new Scaffold(
      //resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: SingleChildScrollView(
            child: Column(
          // Column is also layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Palabras del castellano al guaraní',
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.grey[800],
                    fontSize: 20.0,
                  ),
                )),
            SizedBox(height: 50),
            Container(
                padding: const EdgeInsets.all(8.0),
                child: Image.network(
                    link_imagen != '' ? link_imagen : link_imagen_no_disponible,
                    height: 250,
                    fit: BoxFit.fill)),
            SizedBox(height: 25),
            Container(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  _traduccion_guarani != ''
                      ? 'En guaraní: ${_traduccion_guarani}'
                      : '',
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.grey[800],
                    fontStyle: FontStyle.italic,
                    fontSize: 25.0,
                  ),
                )),
            //SearchForm(),
            Container(
                padding: const EdgeInsets.all(10.0),
                child: new Form(
                    key: formKey,
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new TextFormField(
                          //controller: textController,
                          decoration: new InputDecoration(
                            hintText: "Escriba algo ikatupa?",
                            border: OutlineInputBorder(),
                            labelText: "Ingrese una palabra",
                          ),
                          validator: (input) =>
                              input.contains('@') ? 'Not a valid input' : null,
                          onSaved: (input) => _palabraBuscada = input,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: RaisedButton(
                                    child: Text('Buscar'),
                                    onPressed: _submit,
                                  )),
                            ]),
                      ],
                    )))
          ],
        )),
      ),
      //bottomNavigationBar: BottomNavigationBar(
      // items: const <BottomNavigationBarItem>[
      //BottomNavigationBarItem(
      //icon: Icon(Icons.add),
      // title: Text('Aumentar'),
      // ),
      // BottomNavigationBarItem(
      //  icon: Icon(Icons.remove),
      // title: Text('Disminuir'),
      // ),
      //],
      //onTap: _changeCounter,
      //currentIndex: _selectedIndex,
      //selectedItemColor: Colors.amber[800],
      //),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  //pruebo poner esto aqui
  void _submit() {
    if (this.formKey.currentState.validate()) {
      this.formKey.currentState.save();
      print(_palabraBuscada);
      readData(_palabraBuscada);
    }
  }

  void readData(String _palabraBuscada) async {
    //DocumentSnapshot snapshot = await db
    //    .collection('traducciones')
    //    .document('1dLOAFHgsTlCUNveARnk')
    //    .get();
    //print(snapshot.data['link_imagen'].toString());
    CollectionReference palabrasRef = db.collection('traducciones');
    //print('obtuve esto: ' + palabrasRef.reference().toString());
    //list.forEach((element) => print(element));
    palabrasRef
        .where('palabra_castellana', isEqualTo: _palabraBuscada.toLowerCase())
        .getDocuments()
        .then((snapshot) {
      if (snapshot.documents.length == 0) {
        link_imagen = link_imagen_no_disponible;
        _traduccion_guarani = '';
        setState(() {
          print('estado cambiado');
          print('traduccion al guarani: no disponible');
        });
      }
      snapshot.documents.forEach((elemento) {
        print(elemento.data.toString());
        link_imagen = elemento.data['link_imagen'].toString();
        _traduccion_guarani = elemento.data['palabra_guarani'].toString();
        print('traduccion al guarani:' + _traduccion_guarani);
        setState(() {
          print('estado cambiado');
          print('traduccion al guarani:' + _traduccion_guarani);
        });
      });
      //return snapshot.documents;
    });

    //link_imagen = snapshot.data['link_imagen'].toString()
  }
}
