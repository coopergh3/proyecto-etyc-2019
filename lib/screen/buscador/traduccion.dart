import 'package:flutter/material.dart';

class Traduccion extends StatelessWidget {

  final String _castellano= 'empanada';
  final String _guarani= 'empanada2';
  final String _imagePath= 'https://www.recetasdesbieta.com/wp-content/uploads/2018/09/masa-empanadilla-1.jpg';

  //Parametros que recibira
  //Traduccion(this._castellano, this._guarani, this.imagePath);

   @override
   Widget build(BuildContext context) {
     return Scaffold(
      appBar: AppBar(
         title: Text("Ta'anga Ñe'e"),
      ),
      body: Column (
       mainAxisAlignment: MainAxisAlignment.start,
       crossAxisAlignment: CrossAxisAlignment.center,
       children: [ 
        Container (
          decoration: BoxDecoration(
            image: DecorationImage(image: ExactAssetImage("assets/images/undisponible.jpg")),
          ),
          //Altura de la imagen
          constraints: BoxConstraints.expand(height: 200.8),
          child: 
            Image.network(_imagePath),
        ),
        SizedBox(height: 100),
         Text(
           "Ñe'e Castellano: ",
           style: TextStyle(
            fontSize: 20.0,
           ),
         ),
         Container (
          padding: const EdgeInsets.fromLTRB(40.0, 12.0, 40.0, 12.0),
          decoration: const BoxDecoration(
            border: Border(
              top: BorderSide(width: 1.0, color: Color.fromRGBO(50, 205, 50, 5)),
              left: BorderSide(width: 1.0, color: Color.fromRGBO(50, 205, 50, 5)),
              right: BorderSide(width: 1.0, color: Color.fromRGBO(50, 205, 50, 5)),
              bottom: BorderSide(width: 1.0, color: Color.fromRGBO(50, 205, 50, 5)),
            ),
            color: Color.fromRGBO(220,220,220,5),
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
          child: Text(
            _castellano,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.bold,
              fontSize: 26.0,
              ),
          ),
         ),
         SizedBox(height: 50),
         Text(
           "Ñe'e Guarani: ",
           style: TextStyle(
            fontSize: 20.0,
           ),
         ),
         Container (
          padding: const EdgeInsets.fromLTRB(40.0, 12.0, 40.0, 12.0),
          decoration: const BoxDecoration(
            border: Border(
              top: BorderSide(width: 1.0, color: Color.fromRGBO(50, 205, 50, 5)),
              left: BorderSide(width: 1.0, color: Color.fromRGBO(50, 205, 50, 5)),
              right: BorderSide(width: 1.0, color: Color.fromRGBO(50, 205, 50, 5)),
              bottom: BorderSide(width: 1.0, color: Color.fromRGBO(50, 205, 50, 5)),
            ),
            color: Color.fromRGBO(220,220,220,5),
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
          child: Text(
            _guarani,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.bold,
              fontSize: 26.0,
              ),
          ),
         ),
       ],
     ),
     );
   }
}