import 'package:flutter/material.dart';

class Buscador extends StatelessWidget {
   @override
   Widget build(BuildContext context) {
     return Scaffold(
      appBar: AppBar(
         title: Text("Ta'anga Ñe'e"),
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            new ListTile(
              title: Text("Ñe'e"),
              trailing: Icon(Icons.home),
              //Falta agregar las pantallas y el comando para ir a ellas
              onTap: () => Buscador(),
            ),
            new Divider(),
          ],
        ),
      ),
      body: new Container (
        child:
        Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          TextField(
            obscureText: false,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Emoi petei ñee',
            ),
            onChanged: (String value) {
              print('value');
            },
            
          ),
          new RaisedButton(
            child: Text("Hekara"),
            onPressed: (){
              print('Buscando');
            },
            ),
        ]
        ),
      ),
     );
   }
}