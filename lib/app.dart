import 'package:flutter/material.dart';
import 'package:taanga_nhee/screen/espanhol_guarani.dart';
import 'package:taanga_nhee/screen/espanhol_guarani.dart';
import 'router.dart' as router;

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        onGenerateRoute: router.generateRoute,
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
        home: MyHome());
  }
}

class MyHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'HomePage',
      debugShowCheckedModeBanner: false,
      home: new Scaffold(
        appBar: AppBar(
          title: Text("Hóga"),
        ),
        body: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                "Ta'anga Ñe'ẽ",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color.fromARGB(250, 112, 128, 144),
                    fontSize: 30,
                  )
              )
            )
            ,Container(
                padding: const EdgeInsets.all(8.0),
                child: Image.network(
                    'https://www.ip.gov.py/ip/wp-content/uploads/2018/08/bandera-paraguaya-clima-sol-1.jpg',
                    height: 250,
                    fit: BoxFit.fill)),
            Container(
              padding: const EdgeInsets.all(8.0),
              decoration: const BoxDecoration(
                color: Color.fromRGBO(100, 220, 220, 0.1),
                borderRadius: BorderRadius.all(Radius.circular(8.0)),
              ),
              child: RaisedButton(
                //color: Color.fromARGB(250, 112, 128, 144),//(173,216,230
                child: Text(
                  "Traducir de castellano a guaraní",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color.fromARGB(250, 112, 128, 144),
                    fontSize: 22,
                    fontStyle: FontStyle.italic,
                  ),
                ),
                onPressed: () =>
                    Navigator.pushNamed(context, 'screen/espanhol_guarani'),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(8.0),
              decoration: const BoxDecoration(
                color: Color.fromRGBO(100, 220, 220, 0.1),
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
              ),
              child: RaisedButton(
                child: Text(
                  "Traducir de guaraní a castellano",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color.fromARGB(250, 112, 128, 144),
                    fontSize: 22,
                    fontStyle: FontStyle.italic,
                  ),
                ),
                onPressed: () =>
                    Navigator.pushNamed(context, 'screen/guarani_espanhol'),
              ),
            ),
          ],
        )),
      ),
    );
  }
}
