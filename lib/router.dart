import 'package:flutter/material.dart';
import 'app.dart';
import 'screen/espanhol_guarani.dart';
import 'screen/guarani_espanhol.dart';

Route<dynamic> generateRoute(RouteSettings settings){
  switch (settings.name) {
    case '/':
      return MaterialPageRoute(builder: (context)=> App());
    case 'screen/espanhol_guarani' :
      print('intentando acceder a screen/espanhol_guarani');
      return MaterialPageRoute(builder: (context)=> Espanhol_guarani()); 
      case 'screen/guarani_espanhol' :
      print('intentando acceder a screen/guarani_espanhol');
      return MaterialPageRoute(builder: (context)=> Guarani_Espanhol()); 
    default:
      return MaterialPageRoute(builder: (context)=> App());
  }
}