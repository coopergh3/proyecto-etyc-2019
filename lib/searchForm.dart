import 'package:flutter/material.dart';
import 'package:taanga_nhee/app.dart';

class SearchForm extends StatefulWidget {
  String searchString;
  SearchForm({this.searchString});

  @override
  _SearchFormState createState() => new _SearchFormState();
}

class _SearchFormState extends State<SearchForm> {
  final textController = new TextEditingController();
  final formKey = GlobalKey<FormState>();
  String _palabraBuscada;

  @override
  Widget build(BuildContext context) {
    return new Container(
        padding: const EdgeInsets.all(8.0),
        child: new Form(
            key: formKey,
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new TextFormField(
                  //controller: textController,
                  decoration: new InputDecoration(
                    hintText:
                        "Enter algo ikatupa? O dijiste acaso ${widget.searchString}",
                    border: InputBorder.none,
                    labelText: "Ingrese una palabra",
                  ),
                  validator: (input) =>
                      input.contains('@') ? 'Not a valid input' : null,
                  onSaved: (input) => _palabraBuscada = input,
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: RaisedButton(
                            child: Text('buscar'),
                            onPressed: _submit,
                          )),
                    ]),
              ],
            )));
  }

  void _submit() {
    if (this.formKey.currentState.validate()) {
      this.formKey.currentState.save();
      print(_palabraBuscada);
    }
  }
}
